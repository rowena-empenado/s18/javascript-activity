/* Session 18 - Activity */

let myPokemon = {
	name: 'Pikachu',
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log('This pokemon tackled target Pokemon.')
	},
	faint: function() {
		console.log("Pokemon fainted")
	}
}


function Pokemon(name, level) {
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = level;
	
	this.tackle = function(target) {
		console.log(this.name + ' tackled ' + target.name);
		target.health = target.health - this.attack;
		/*console.log(target.health)*/
		console.log(target.name + "'s health is now reduced to " + target.health);
		if (target.health <= 5) {target.faint();}
	},

	this.faint = function() {
		if (this.health <= 5) {console.log(this.name + ' fainted.');}
	}	
}


let pikachu = new Pokemon('Pikachu', 16);
let squirtle = new Pokemon('Squirtle', 8);

console.log(pikachu);
console.log(squirtle);

pikachu.tackle(squirtle); // 24 - 16 = 8
pikachu.tackle(squirtle); // 8 - 16 = -8
